# About You Coding Assignment
This is a pretty simplified docker-compose workflow that sets up a LEMP network of containers for local Laravel development.


## Setup

To get started, make sure you have [Docker installed](https://docs.docker.com/docker-for-mac/install/) on your system.

Please follow the steps below:

Open a terminal and clone this repository.

    git clone https://bitbucket.org/aly9/au-coding-task.git

Go into *au-coding-task* directory

    cd au-coding-task

Build docker composer for the first time and then pull it up.

    docker-compose build && docker-compose up -d

Go into *src* directory

    cd src

Install the dependencies using composer, `composer install` or if you don't have the composer installed then do it via composer docker command: 

    sudo docker run --rm -v "$(pwd):/app" composer install

Give permissions to *storage* and *bootstrap/cache* directories

    chmod 777 -R storage

    chmod 777 -R bootstrap/cache

Copy the .env.example file to .env file.

Generate encryption key for laravel app.

    docker-compose run --rm php php /var/www/artisan key:generate

Open up your browser of choice to [http://localhost:8080](http://localhost:8080) and you should see your Laravel app running as intended. 

## Ports Conflicts
Containers created and their ports are as follows:

- **nginx** - `:8080`
- **mysql** - `:3306`
- **php** - `:9000`

**NOTE:** You might have to change the prots for these containers if you these ports are already in use.

## Usage

### Option-01

To multiply two matrices you can use the artisan command below

    docker-compose run --rm php php /var/www/artisan matrix:multiply matrixAJson matrixBJson

In this command **matrixAJson** is suppose to be the string representation of a 2-D array for example [[1,2], [3,4]] will be interpreted as a matrix of 2X2, and the same goes for **matrixBJson**. An example command in usage will be:

    docker-compose run --rm php php /var/www/artisan matrix:multiply [[1]] [[27]]


This will output **AA**

Have a look at the screenshot for visual understanding.
![matrix multiply](https://i.ibb.co/YR8dTsV/Whats-App-Image-2019-08-20-at-3-28-39-AM.jpg)

### Option-02

**NOTE: Postman collection and environment is added in the repository for convenience have a look into src/Postman**:

You can also use the rest API for the purpose of matrix multiplication.
- Post your matrix data as JSON arrays in the body of rest api request.
- Content-type should be *application/json*

**Api Url**:

    http://localhost:8080/matrix/multiply

**Sample Data**:

```
[
	[
		[1,2,3],
		[4,5,6],
		[7,8,9]
	],
	[
		[1,2,3],
		[4,5,6],
		[7,8,9]
	]
]
```

## Tests

### Unit Tests
Please execute the below command to run all Unit tests.

    docker-compose run --rm php /var/www/vendor/bin/phpunit /var/www/tests/Unit

### Load Tests
I've also added one load test in order to verify the limits of this solution and it works for a matrix of **1000 X 1000**
The test was completed in 2.55 minutes.

    docker-compose run --rm php /var/www/vendor/bin/phpunit /var/www/tests/Load

## Todos
Following are the things which couldn't be done due to time constraints but should be implemented in future for a scalable and more performant solution.

- Add more intuitive way for matrix inputs, maybe use text files for bigger matrices inputs.
- Improve the algorithm to make benefit out of PHP 7's concurrency.
- For huge matrices use [Divide and Conquer algorithm](https://en.wikipedia.org/wiki/Matrix_multiplication_algorithm#Divide_and_conquer_algorithm)
- Introduce the use of concurrent process to make use of all cores of CPU
- Add more tests for 100% test coverage