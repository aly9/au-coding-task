<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Factories\MatrixFactory;
use App\Matrix;

class MatrixTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->A = [
            [1, 2, 3],
            [2, 3, 4],
            [4, 5, 6],
        ];
        $this->matrix = MatrixFactory::create($this->A);
    }

    /**
     * @testCase constructor
     */
    public function testConstructorTest()
    {
        $this->assertInstanceOf(Matrix::class, $this->matrix);
    }

    /**
     * @testCase constructor throws \App\Exceptions\MatrixException if the number of columns is not consistent
     */
    public function testConstructorExceptionNCountDiffersTest()
    {
        $A = [
            [1, 2, 3],
            [2, 3, 4, 5],
            [3, 4, 5],
        ];
        $this->expectException(\App\Exceptions\MatrixException::class);
        $matrix = MatrixFactory::create($A);
    }

    /**
     * @testCase constructor throws \App\Exceptions\PoorDataException if the number of columns is not consistent
     * @return [type] [description]
     */
    public function testClassConstructorExceptionCountDiffersTest()
    {
        $A = [
            [1, 2, 3],
            [2, 3, 4, 5],
            [3, 4, 5],
        ];
        $this->expectException(\App\Exceptions\PoorDataException::class);
        $matrix = new Matrix($A);
    }
}
