<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Factories\MatrixFactory;

class MatrixMultiplicationLoadTest extends TestCase
{
    protected function getBigArraysForMatrix($rows=1000, $cols=1000, $min=1, $max=1000)
    {
        $array = [];
        for ($r=0; $r<$rows ; $r++) { 
            for ($c=0; $c<$cols ; $c++) { 
                $array[$r][$c] = rand($min, $max);
            }
        }
        return $array;
    }

    protected function setUp(): void
    {
        parent::setUp();
        $array = self::getBigArraysForMatrix();
        $this->SA = $array;
        $this->SB = $array;
        
        $this->matrixSA = MatrixFactory::create($this->SA);
        $this->matrixSB = MatrixFactory::create($this->SB);
    }

    /**
     * @testCase Square Matrices Multiplication test
     */
    public function testBigSquareMatricesMultiplicationTest()
    {
        $resultantMatrix = $this->matrixSA->multiply($this->matrixSB);
        
        $this->assertTrue(
            $resultantMatrix->getRows() == $this->matrixSA->getRows()
        );

        $this->assertTrue(
            $resultantMatrix->getCols() == $this->matrixSB->getCols()
        );
    }
    
}
