<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Factories\MatrixFactory;

class MultiplyMatrices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'matrix:multiply 
        {matrixAJson : The json string containing 2 dimensional array}
        {matrixBJson : The json string containing 2 dimensional array}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will multiply two matrices.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $matrixA = MatrixFactory::create($this->argument('matrixAJson'));
        $matrixB = MatrixFactory::create($this->argument('matrixBJson'));
        
        $resultantMatrix = $matrixA->multiply($matrixB);
        echo $resultantMatrix->toExcel();
    }
}
