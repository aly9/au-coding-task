<?php

namespace App\Helpers;

use App\Exceptions\InvalidNumberException;

trait Excel
{
    /**
     * Converts a given integer to Excel Column Naming Series
     *
     * @param integer $number The integer to be converted
     * @throws InvalidNumberException Wrong parameters
     */
    public function getExcelNameFromNumber($number): string
    {
        if($number < 1) {
            throw new InvalidNumberException('Integer should be 1 or greater');
        }
        $numeric = ($number - 1) % 26;
        $letter = chr(65 + $numeric);
        $num2 = intval(($number - 1) / 26);
        if ($num2 > 0) {
            return $this->getExcelNameFromNumber($num2) . $letter;
        } else {
            return $letter;
        }
    }

}
