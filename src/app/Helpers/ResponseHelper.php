<?php

namespace App\Helpers;

trait ResponseHelper
{
    /**
     * Sends an array for API response
     *
     * @param string $message The error message to be sent in response
     * @param array $data The data along the error message, if any
     * @return array The array to be sent in response
     */
    public function errorResponse($message, $data=[]): array
    {
        return [
            'success' => false,
            'message' => $message,
            'data' => $data,
        ];
    }

    /**
     * Sends an array for API response
     *
     * @param string $message The error message to be sent in response
     * @param array $data The data along the error message, if any
     * @return array The array to be sent in response
     */
    public function successResponse($message, $data=[]): array
    {
        return [
            'success' => true,
            'message' => $message,
            'data' => $data,
        ];
    }

}
