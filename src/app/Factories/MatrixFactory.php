<?php

namespace App\Factories;

use App\Matrix;
use App\Exceptions\InvalidTypeException;
use App\Exceptions\PoorDataException;
use App\Exceptions\MatrixException;
use App\Exceptions\InvalidNumberException;
use PhpParser\Node\Expr\Array_;

/**
 * Matrix factory to create matrices from different type if inputs.
 * Use factory instead of instantiating individual Matrix classes.
 * A Matrix can be created with A 2-D Array
 */
class MatrixFactory
{
    /**
     * Factory method
     *
     * @param  mixed $A 2-dimensional array of Matrix data
     *
     * @return Matrix
     *
     * @throws PoorDataException
     * @throws InvalidTypeException
     * @throws MatrixException
     */
    public static function create($A): Matrix
    {
        if(is_string($A)){
            $A = self::createArrayFromJson($A);
        }
        self::checkParams($A);

        if(self::isValidate2dArray($A)) {
            return new Matrix($A);
        }
        throw new InvalidTypeException('Unknown matrix type');
    }

    /**
     * Convert json to Array
     *
     * @param  String $text 2-dimensional array string of Matrix data
     *
     * @return Matrix
     *
     * @throws PoorDataException
     */
    private static function createArrayFromJson($text): Array
    {
        if(empty($text)){
            throw new PoorDataException('Given JSON cannot be empty');
        }
        $matrixArray = json_decode($text);
        if(json_last_error() != JSON_ERROR_NONE) {
            throw new PoorDataException('Given JSON is invalid');
        }
        return $matrixArray;
    }

    /**
     * Check input parameters
     *
     * @param array $A
     *
     * @return bool
     *
     * @throws PoorDataException if array data not provided for matrix creation
     * @throws MatrixException if any row has a different column count
     * @throws InvalidNumberException if any number is out of range
     */
    private static function checkParams(array $A): bool
    {
        if (empty($A)) {
            throw new PoorDataException('Array data not provided for Matrix creation');
        }
        if (isset($A[0]) && is_array($A[0])) {
            $column_count = count($A[0]);
            foreach ($A as $i => $row) {
                if (count($row) !== $column_count) {
                    throw new MatrixException("Row $i has a different column count: " . count($row) . "; was expecting $column_count.");
                }
                foreach($row as $value) {
                    if(self::isOutOfRange($value)) {
                        throw new InvalidNumberException("The given value $value is out of range");
                    }
                }
            }
        }
        return true;
    }

    /**
     * Determine if the matrix given array is a 2-D array
     *
     * @param array[] $A 2-dimensional array of Matrix data
     *
     * @return bool indicating if $A is a valid 2D array or not
     */
    private static function isValidate2dArray(array $A): bool
    {
        return (is_array($A[0][0])) ? false : true;
    }

    /**
     * Determine if the given numer is inside PHP integer's range
     *
     * @param mixed $number The integer supposed to be validated
     *
     * @return bool indicating if $number is valid or not
     */
    public static function isOutOfRange($number): bool
    {
        return ( (PHP_INT_MIN >= $number) || ($number >= PHP_INT_MAX) )  ? true : false;
    }
}