<?php

namespace App;

use App\Exceptions\MatrixException;
use App\Exceptions\IncompatibleMatrixException;
use App\Exceptions\InvalidNumberException;
use App\Exceptions\PoorDataException;
use App\Helpers\Excel;
use App\Factories\MatrixFactory;
use ArrayObject;

class Matrix extends ArrayObject
{
    use Excel;

    /**
     * Number of rows in the matrix.
     *
     * @var int
     */
    private $_rows;

    /**
     * Number of columns in the matrix.
     *
     * @var int
     */
    private $_cols;

    /**
     * Create a matrix from another matrix, an array or with its size (rows, cols)
     *
     * @param mixed $value Matrix, array or number of rows
     * @throws MatrixException Wrong parameters
     */
    public function __construct($value, $cols = null)
    {
        if ($value instanceof self) {
            parent::__construct($value);
            $this->_rows = $value->_rows;
            $this->_cols = $value->_cols;
        } else if (is_array($value) && $cols == null) {
            parent::__construct($value);
            $this->_rows = count($value);
            $this->_cols = count($value[0]);

            foreach ($value as $i => $row) {
                if (count($row) !== $this->_cols) {
                    throw new PoorDataException("Row $i has a different column count: " . count($row) . "; was expecting {$this->_cols}.");
                }
            }
        } else if (is_numeric($value) && is_numeric($cols)
            && $value > 0 && $cols > 0
        ) {
            $this->_rows = $value;
            $this->_cols = $cols;
            for ($r = 0; $r < $this->_rows; $r++) {
                $this[$r] = [];
                for ($c = 0; $c < $this->_cols; $c++) {
                    $this[$r][$c] = 0;
                }
            }
        } else {
            throw new MatrixException('Cannot create matrix');
        }
    }

    /**
     * Multiply another matrix or a scalar to this matrix,
     * return a new matrix instance on success or
     * throws exceptions.
     *
     * @param Matrix $matrix matrix to multiply to this matrix
     * @return Matrix New result matrix
     * @throws IncompatibleMatrixException If matrices are incompatible
     * @throws InvalidNumberException If any two integers product is out of range
     */
    public function multiply(Matrix $matrix): Matrix
    {
        if ($this->_cols != $matrix->_rows) {
            throw new IncompatibleMatrixException('Cannot multiply matrices: incompatible matrices');
        }

        // ikj algorithm
        $resultArray = [];
        for ($i = 0; $i < $this->_rows; $i++) {
            $resultArray[$i] = array_fill(0, $matrix->_cols, 0);
            for ($k = 0; $k < $this->_cols; $k++) {
                for ($j = 0; $j < $matrix->_cols; $j++) {
                    $_product = $this[$i][$k] * $matrix[$k][$j];
                    // Throw exception if $_product is out of range
                    if( MatrixFactory::isOutOfRange($_product) ) {
                        throw new InvalidNumberException('Integer out of range.');
                    }
                    
                    $resultArray[$i][$j] += $_product;
                }
            }
        }
        
        return MatrixFactory::create($resultArray);
    }

    /**
     * Returns human readable matrix string like a pseudo table.
     *
     * @return string The matrix
     */
    public function __toString(): string
    {
        $out = '';
        for ($r = 0; $r < $this->_rows; $r++) {
            for ($c = 0; $c < $this->_cols; $c++) {
                if ($c) {
                    $out .= "\t";
                }
                $out .= $this[$r][$c];
            }
            $out .= "\n";
        }
        return $out;
    }

    /**
     * Returns human readable matrix string like a pseudo table.
     *
     * @return string The matrix
     */
    public function toExcel(): string
    {
        $out = '';
        for ($r = 0; $r < $this->_rows; $r++) {
            for ($c = 0; $c < $this->_cols; $c++) {
                if ($c) {
                    $out .= "\t";
                }
                $out .= $this->getExcelNameFromNumber($this[$r][$c]);
            }
            $out .= PHP_EOL;
        }
        return $out;
    }

    /**
     * Returns boolean if a matrix is equal to the other. 
     *
     * @param Matrix $matrix matrix to multiply to this matrix
     * @return bool Either tha matrix is equal or not
     */
    public function isEqual(Matrix $matrixB): bool
    {
        $m = $this->_rows;
        $n = $this->_cols;
        
        // if dimensions are not same
        for ($r = 0; $r < $this->_rows; $r++) {
            for ($c = 0; $c < $this->_cols; $c++) {
                if($this[$r][$c] != $matrixB[$r][$c]) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Returns Number of rows 
     *
     * @return integer Number of rows of current matrix
     */
    public function getRows()
    {
        return $this->_rows;
    }

    /**
     * Returns Number of cols 
     *
     * @return integer Number of cols of current matrix
     */
    public function getCols()
    {
        return $this->_cols;
    }
}