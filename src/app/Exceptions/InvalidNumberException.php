<?php
namespace App\Exceptions;

use Exception;

final class InvalidNumberException extends Exception
{
}
