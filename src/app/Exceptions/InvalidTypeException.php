<?php
namespace App\Exceptions;

use Exception;

final class InvalidTypeException extends Exception
{
}
